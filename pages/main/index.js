import React from "react";
import { useState } from "react";
import XLSX from "xlsx";
import path from "path";
import { useRouter } from "next/router";
import Link from "../../src/Link";
import NavigationBar from "../../components/NavigationBar/NavigationBar";

export default function Index({ sheet_Contents }) {
    return (
        <>
            {/* <h1>鄧佈利多的秘密</h1> */}
            <NavigationBar NavItems={sheet_Contents} />;
        </>
    );
}
export async function getServerSideProps() {
    // use XLSX package to read excel
    const wb = XLSX.readFile(
        path.join("DataSource", "鄧佈利多的秘密_20220415v3.xlsm")
    );
    // list the sheets from excel
    const sheet_Lists = wb.SheetNames;
    // remove the lastest 2 sheets from array (DB uses)
    sheet_Lists.length = sheet_Lists.length - 2;

    // map array to get the data by per one sheet, return sheetName and data
    // output fromat is html
    const sheet_Contents = sheet_Lists.map((sheetName, index) => {
        const sheet_Data = wb.SheetNames[index];
        const work_sheet = wb.Sheets[sheet_Data];
        const html_Data = XLSX.utils.sheet_to_html(work_sheet);
        return { sheetName, html_Data };
    });

    // return objects to be used by react rendering.
    return {
        props: { sheet_Contents },
    };
}
