import React from "react";
import { useState } from "react";
import XLSX from "xlsx";
import path from "path";
import { useRouter } from "next/router";
import Link from "../../src/Link";
import NavigationBar_2 from "../../components/NavigationBar_2/NavigationBar_2";
import style from "./main2.module.css";

export default function Index({ sheet_Contents, html_Data }) {
    return (
        <>
            {/* <h1>鄧佈利多的秘密</h1> */}
            <NavigationBar_2 NavItems={sheet_Contents} />
            <div
                className={style["data-body"]}
                dangerouslySetInnerHTML={{ __html: html_Data }}
            />
        </>
    );
}
export async function getServerSideProps(context) {
    const { params, query } = context;
    // console.log(query); //output: { main: 'CB套利' }
    // console.log(params.main); output: CB套利

    // use XLSX package to read excel
    const wb = XLSX.readFile(
        path.join("DataSource", "鄧佈利多的秘密_20220415v3.xlsm")
    );
    // list the sheets from excel
    const sheet_Lists = wb.SheetNames;
    // remove the lastest 2 sheets from array (DB uses)
    sheet_Lists.length = sheet_Lists.length - 2;

    const loadingPage_index = sheet_Lists.indexOf(params.main2);
    const sheet_Data = wb.SheetNames[loadingPage_index];
    const work_sheet = wb.Sheets[sheet_Data];
    const html_Data = XLSX.utils.sheet_to_html(work_sheet);

    // get sheetName for navigationbar use
    const sheet_Contents = sheet_Lists.map((sheetName, index) => {
        return { sheetName };
    });
    // return objects to be used by react rendering.
    return {
        props: { sheet_Contents, html_Data },
    };
}
