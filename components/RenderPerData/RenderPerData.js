import React, { useState } from "react";

export default function RenderPerData({ inputData, index }) {
    const [renderData, setRenderData] = useState(false);
    const showRenderData = () => setRenderData(!renderData);

    return (
        <>
            {/* <button onClick={showRenderData}>{inputData.sheetName}</button> */}
            {/* <a onClick={showRenderData}>{inputData.sheetName}</a> */}
            {/* {renderData && (
                <div
                    dangerouslySetInnerHTML={{
                        __html: inputData.html_Data,
                    }}
                />
            )} */}
            <div
                dangerouslySetInnerHTML={{
                    __html: inputData.html_Data,
                }}
            />
        </>
    );
}
