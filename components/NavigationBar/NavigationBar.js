import React from "react";
import style from "./NavigationBar.module.css";
import Link from "../../src/Link";

export default function NavigationBar({ NavItems }) {
    return (
        <>
            <nav className={style["nav-bar"]}>
                {NavItems.map((NavItem, index) => {
                    return (
                        <Link href={`/main/${NavItem.sheetName}`} key={index}>
                            {NavItem.sheetName}
                        </Link>
                    );
                })}
            </nav>
        </>
    );
}
