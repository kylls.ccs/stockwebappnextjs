import React from "react";

export default function StockCard({ obj }) {
    const newObj_array = Object.keys(obj).map((key) => {
        return [key, obj[key]];
    });
    // newObj_array.shift()
    return (
        <>
            {newObj_array.map((text, index) => {
                return (
                    <a className="" key={index}>
                        {text[1]}
                    </a>
                );
            })}
        </>
    );
}
