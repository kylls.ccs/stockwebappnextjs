import React from "react";
import style from "./NavigationBar_2.module.css";
import Link from "../../src/Link";

export default function NavigationBar_2({ NavItems }) {
    return (
        <>
            <nav className={style["nav-bar"]}>
                {NavItems.map((NavItem, index) => {
                    return (
                        <Link href={`/main2/${NavItem.sheetName}`} key={index}>
                            {NavItem.sheetName}
                        </Link>
                    );
                })}
            </nav>
        </>
    );
}
