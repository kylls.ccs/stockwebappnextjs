import React from "react";
import { useState } from "react";
import XLSX from "xlsx";
import path from "path";
import { useRouter } from "next/router";
import Link from "../../src/Link";
import NavigationBar from "../../components/NavigationBar/NavigationBar";
import style from "./main.module.css";
import StockCard from "../../components/StockCard/StockCard";

export default function Index({ sheet_Contents, html_Data, json_Data }) {
    const json_Data_title = json_Data[0];
    const json_Data_body = json_Data.slice(1);
    // console.log(json_Data_title);
    return (
        <>
            <NavigationBar NavItems={sheet_Contents} />
            <div className={style["column-title"]}>
                <StockCard obj={json_Data_title} />
            </div>
            {json_Data_body.map((text, index) => {
                return (
                    <div className={style["data-container"]}>
                        <StockCard obj={text} key={index} />
                    </div>
                );
            })}
        </>
    );
}
export async function getServerSideProps(context) {
    const { params, query } = context;
    // console.log(query); output: { main: 'CB套利' }
    // console.log(params.main); output: CB套利

    // use XLSX package to read excel
    const wb = XLSX.readFile(
        path.join("DataSource", "鄧佈利多的秘密_20220420v1.xlsm")
    );
    // list the sheets from excel
    const sheet_Lists = wb.SheetNames;
    // remove the lastest 2 sheets from array (DB uses)
    sheet_Lists.length = sheet_Lists.length - 2;

    const loadingPage_index = sheet_Lists.indexOf(params.main);
    const sheet_Data = wb.SheetNames[loadingPage_index];
    const work_sheet = wb.Sheets[sheet_Data];
    const html_Data = XLSX.utils.sheet_to_html(work_sheet);
    const json_Data = XLSX.utils.sheet_to_json(work_sheet);
    const json_Data_title = json_Data[0];
    const json_Data_Content = json_Data.slice(1);
    // json_Data.shift();

    // get sheetName for navigationbar use
    const sheet_Contents = sheet_Lists.map((sheetName, index) => {
        return { sheetName };
    });
    // return objects to be used by react rendering.
    return {
        props: { sheet_Contents, html_Data, json_Data },
    };
}
